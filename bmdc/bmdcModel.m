function [modelLik modelDensity logDetSigs] = ...
                        bmdcModel(data,latents,hypers,modelOptions,varargin)
if isfield(modelOptions,'dataNu')
    dataNu=modelOptions.dataNu; %y_t ~ Student(nu,Sigma_t)
else
    dataNu=0; %y_t ~ N(0,Sigma_t)
end

[T d] =size(data);
sigSize =size(latents);
N=sigSize(1);

Sigmas = reshape(latents,[N d d]);
if d==1
    Sigmas=shiftdim(Sigmas,-2);
else
    Sigmas=shiftdim(Sigmas,1);
end

if dataNu %t-distributed
    modelLik = zeros(T,N); logDetSigs = zeros(T,N); modelDensity = zeros(T,N); 
    nu=hypers(:,end);
    
    %y_t ~ Student(nu,0,S_t)
    %Sigma_t=nu/(nu-2) S_t
    for t=1:T
        for i=1:N
            covAdj= (nu(i)-2)./nu(i);
            [curLik curDensity curDetSig]=mvtpdfln(data(t,:),Sigmas(:,:,i)*covAdj,nu(i),[]);
            modelLik(t,i) = curLik;
            modelDensity(t,i) = curDensity;
            logDetSigs(t,i)=curDetSig;
        end
    end
    
    if modelOptions.modelSetup~=1 %particle filter
        modelLik=modelLik'; %expects N*1 (T=1)
        modelDensity=modelDensity'; %expects N*1 (T=1)
        logDetSigs=logDetSigs'; %expects N*1 (T=1)
    end
else
    [modelLik modelDensity logDetSigs] =mvnpdfln(data,[],Sigmas,0);
end
function [predLatents predLik logDetSigs] = ...
    bmdcPredFun(predData, curData, newParams, curLatents, modelOptions,varargin)

[T d]=size(predData);
latSize=size(curLatents); 
N=latSize(1);

curLatents=reshape(curLatents,[N d d]);
if d==1
    curLatents=shiftdim(curLatents,-2);
else
    curLatents=shiftdim(curLatents,1);
end

predLatents= bmdcSigma(curData,newParams,curLatents,modelOptions);
predLatents = shiftdim(predLatents,2); %conform with N*latentSize 
[predLik predDensity logDetSigs]=...
    bmdcModel(predData,predLatents,newParams,modelOptions,varargin);
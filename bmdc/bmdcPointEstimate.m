function [m sigmaMean] = ...
    bmdcPointEstimate(data,prevParams,prevLatents,prevDensity,pfOptions,varargin)
% defaults
if isfield(pfOptions.modelOptions,'isDynamic')
    isDynamic = pfOptions.modelOptions.isDynamic;
else
    isDynamic = 0;
end
if isfield(pfOptions,'delta')
    delta=pfOptions.delta;
else
    delta=.95; % suggested by Liu and West
end

[t,d]=size(data);
[N numParams]=size(prevParams);
d2 = d*(d+1)/2; numEvoParams = d2+2*d;

sigSize =size(prevLatents);
N=sigSize(1);
prevLatents= reshape(prevLatents,[N d d]);
if d==1
    prevLatents=shiftdim(prevLatents,-2);
else
    prevLatents=shiftdim(prevLatents,1);
end

temp=repmat(exp(prevDensity),1,numParams).*prevParams;
paramMean = sum(temp)/N;

a=(3*delta-1)/(2*delta); 
m = a*prevParams+(1-a)*repmat(paramMean,N,1);

if ~isDynamic %static params -> kernelized smoothing for shared parameters
    pfOptions.modelOptions.wishNu=0;
    sigmaMean = bmdcSigma(data(t-1,:),prevParams,prevLatents,pfOptions.modelOptions); %is predicted latent
else %dynamic params -> kernelized smoothing for jitter parameters
    %only pointEstimate on hyper params
    m(:,1:numEvoParams)=prevParams(:,1:numEvoParams); %revert to particles    
    sigmaMean = prevLatents; %predicted doesn't change given new hypers
end
sigmaMean=shiftdim(sigmaMean,2);
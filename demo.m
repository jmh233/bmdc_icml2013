% rapf + bmdc demo
clear
addpath(genpath('.'))

% generate according to BMDC data
numStates=100;
d=5; 
d2=d*(d+1)/2;
AFlat = ones(d,1)*1; %BMDC has diagonal A, B and upper triangular C
BFlat = ones(d,1)*0;
CFlat = ones(d2,1)*0;

options.isDynamic=1; %jitter only used if isDynamic
jitter=[.001; .001; .005];
if options.isDynamic
    params = [CFlat; BFlat; AFlat; jitter]'; %want 1*numParams
else % this is BEKK-like
    params = [CFlat; BFlat; AFlat; ]'; %want 1*numParams
end

options.dataNu=0; %0 for gaussian data; else Student-t data
if options.dataNu
    params = [params options.dataNu];
end

[simData SigmaT params]= generateBMDCData(params,d,numStates,options);
plot(simData)

% run rapf
pfFuncs.likFunc = @bmdcModel;
% pfFuncs.condLikFunc=@bmdcLatentModel;
pfFuncs.paramProposal=@bmdcParamProposal;
pfFuncs.pointEstimate=@bmdcPointEstimate;
pfFuncs.predFunc=@bmdcPredFun;

pfOptions.numParticles=1000; 
pfOptions.resampleOption= 1; %1= systematic; 2=multinomial; 3 = residual; 4 = stratified
pfOptions.learnParams = 2; %0= dont learn; 1=SMC; 2 = regularised

isDynamic=1; %0 for BEKK-like behaviour
pfOptions.modelOptions.isDynamic=isDynamic;
if isDynamic       
    pfOptions.modelOptions.tau = [.001 .001 .001];
end

pfOptions.modelOptions.dataNu=0;
% pfOptions.modelOptions.dataNu=8; %Student with 8 degrees of freedom
pfOptions.modelOptions.absIdentify=1; %identify parameters
    
initParams = []; % empty or prior mean 
%header given to be saved
[latentParts paramParts predLik summaryStats] = ...
    rapf(simData,initParams,pfFuncs,pfOptions);
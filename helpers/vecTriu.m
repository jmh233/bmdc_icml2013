function vecData=vecTriu(x,upperFlag)
% vectorize triangular matrix
if nargin<2
    upperFlag=1; %upper by default
end

if upperFlag
    xTri = triu(ones(size(x)));
else
    xTri = tril(ones(size(x)));
end
vecData=x(logical(xTri));
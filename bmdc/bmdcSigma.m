function [sigma] = bmdcSigma(data,params,prevSigmas,modelOptions)
if nargin<4
    modelOptions=[];
end
if isfield(modelOptions,'dataNu')
    dataNu = modelOptions.dataNu;
else
    dataNu=0;
end

[T d]=size(data);
d2 = d*(d+1)/2;
[N numParams] = size(params);

sigma = prevSigmas; %for sizing purposes
for i=1:N
    C = vec2Mat(params(i,1:d2));
    if isfield(modelOptions,'isFull') && modelOptions.isFull
        B = reshape(params(i,d2+1:d2+d^2),[d d]);
        A = reshape(params(i,d2+d^2+1:d2+2*d^2),[d d]);
    else %diagonal
        B = diag(params(i,d2+1:d2+d));
        A = diag(params(i,d2+d+1:d2+2*d));
    end
        
    arPart = A*prevSigmas(:,:,i)*A';
    maPart = B*(data'*data)*B';
    intPart = C'*C;

    S = arPart+maPart+intPart;

    sigma(:,:,i)=S;
end
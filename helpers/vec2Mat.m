function dataMatrix = vec2Mat(data,r,c,upperFlag)
% take vectorized data and turn into upper or lower triangular matrix
if nargin<4
    upperFlag=1;
end
if nargin<2
    r=floor(sqrt(length(data)*2)); % l=r(r+1)/2 for square matrix
end
if nargin<3
    c=r; %numColumns= numRows
end

dataMatrix = zeros(r,c);
counter = 1;
for i=1:c    %since matlab always vectorizes down columns
    counterEnd = counter+i-1;
    dataMatrix(1:i,i)=data(counter:counterEnd);
    counter = counterEnd+1;
end

if ~upperFlag
    dataMatrix=dataMatrix';
end
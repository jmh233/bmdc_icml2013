function [normWgts Neff] = normalizeWeights(logDensity)
% logDensity(isnan(logDensity))=-Inf;
maxLogDensity = max(logDensity);
wgts = exp(logDensity - maxLogDensity ); %adjust for numerical stability    
wgts(imag(wgts)~=0)=0; %nan or imaginary wgts is treated as bad support or low probability in code
normWgts = wgts/nansum(wgts); 
normWgts(isnan(normWgts))=0;
Neff  = 1/sum(normWgts.^2); %track effective sample size    
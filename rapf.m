function [xFilt paramFilt predLik summaryStats] = rapf(data,paramsPrior,pfFuncs,pfOptions,varargin)
%% general rapf method
% comments will illustrate how to use this for BMDC
% y_t ~ f(x_t,params); y observations/data
% x_t ~ g(x_{1:t-1},y_{1:t-1},params); hidden states
% params - system parameters that can be learnt

% some initial checks
if ~isfield(pfFuncs, 'paramProposal')
    error('please specify importance density; q(x_t | D)')
end
if ~isfield(pfFuncs, 'likFunc')
    error('please specify model: p(y|x,theta)')
end
if ~isfield(pfFuncs, 'pointEstimate')
    error('please specify pointEstimate for apf')
end

% defaults
if ~isfield(pfOptions,'resampleOption')
    resampleOption=1; %uses systematic resampling by default
else
    resampleOption=pfOptions.resampleOption;
end

%alias sizes
dataSize = size(data);
T = dataSize(1);
N = pfOptions.numParticles;

% parameter identification code
d=dataSize(2);
pascalNumbers = [1:d].*([1:d]+1)/2;
if isfield(pfOptions.modelOptions,'isFull') && pfOptions.modelOptions.isFull
    pascalNumbersB=[1:d].^2+d*(d+1)/2;
    pascalNumbersA=[1:d].^2+d*(d+1)/2+d*d;
    diagIndex = [pascalNumbers pascalNumbersB pascalNumbersA];
else %only identify C
    numEvoParams = d*(d+1)/2+d*2;
    diagIndex = [pascalNumbers (pascalNumbers(end)+1):numEvoParams];
end

% initial latents and params
prevDensity=zeros(N,1); %log density -equal weighted initially
[xNew qXln paramsNew qParamsln] = pfFuncs.paramProposal(data,[],paramsPrior,pfOptions,varargin{:});
latentDim = size(xNew);
paramDim = size(paramsNew);

% output structures with initiation
xFilt = repmat(shiftdim(xNew,-1),[T 1]); 
paramFilt = repmat(shiftdim(paramsNew,-1),[T 1]);
predLik2 = zeros(T,N);

% stats
summaryStats.latentMean= zeros([T,latentDim(2:end)]); 
summaryStats.paramMean= zeros([T,paramDim(2:end)]); 
summaryStats.paramMean(1,:) = mean(paramsNew,1);
summaryStats.numEffs= zeros(T,1);
summaryStats.timeUsed = zeros(T,1);

for t=2:T
    tStart=tic;
    
    %calculate point estimates (mean/mode) for Sigma_t, theta_t
    [m xAdj] = pfFuncs.pointEstimate(data(1:t,:),paramsNew,xNew,prevDensity,pfOptions,varargin{:});
    
    %calculate prevParticle weights
    %w_t=w_{t-1}*p(y_t|mu_t,m_t-1)
    modelDensityMu = pfFuncs.likFunc(data(t,:),xAdj,m,pfOptions.modelOptions,varargin{:});
    newDensity = prevDensity+modelDensityMu; %log densities

    %sample auxiliary variable according to normalized weights
    [normWgts Neff] = normalizeWeights(newDensity);
    [pIndex] = resample(normWgts,resampleOption);

    %sample theta_t according to kernel and auxiliary index 
    [xNew qXln paramsNew qParamsln]= pfFuncs.paramProposal(data(1:t,:),xNew(pIndex,:),m(pIndex,:),pfOptions,varargin{:});
    
    %compute weights for new samples := w_t=w_{t-1}*p(y_t|x_t,theta_t-1)
    modelDensity=pfFuncs.likFunc(data(t,:),xNew,paramsNew,pfOptions.modelOptions,varargin{:});
    % match against auxiliary index
    prevDensity = modelDensity-modelDensityMu(pIndex,:); 
     
    %stats
    weights = repmat(exp(prevDensity),[1 latentDim(2:end)]);
    temp = shiftdim(nansum(xNew.*weights,1),1)/N;
    summaryStats.latentMean(t,:) = temp(:);
    summaryStats.numEffs(t,:)=Neff;
    
    if isfield(pfOptions.modelOptions, 'absIdentify') && pfOptions.modelOptions.absIdentify
        temp = paramsNew;
        temp(:,diagIndex) = abs(temp(:,diagIndex)); %identifiability on diagonal
        weights = repmat(exp(prevDensity),[1 size(paramsNew,2)]);
        summaryStats.paramMean(t,:) = nansum(temp.*weights)/N;
    else
        weights = repmat(exp(prevDensity),[1 size(paramsNew,2)]);
        summaryStats.paramMean(t,:) = nansum(paramsNew.*weights)/N;
    end
    
    %predictive likelihood - predict 1 step forward
    if t<T && ~isempty(pfFuncs.predFunc)
        [predX predLikCur]=pfFuncs.predFunc(data(t+1,:),data(t,:),paramsNew,xNew,pfOptions.modelOptions,varargin{:});
        predLik2(t+1,:)= predLikCur;
    end
    
    summaryStats.timeUsed(t)=toc(tStart); % log time used for each state
end
predLik=logSum(predLik2);
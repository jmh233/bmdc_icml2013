This is the code for the paper

Wu Y., Hernandez-Lobato J. M. and Ghahramani Z.
Dynamic Covariance Models for Multivariate Financial Time Series,
In ICML, 2013

You can test the method described in the above paper by running the script
demo.m

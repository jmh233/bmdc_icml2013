function[simData SigmaT params]= ...
    generateBMDCData(params,d,numStates,options,startSigma,startData)
% model is :
% y_t ~ N(0, Sigma_t) or Student(nu,Sigma_t)
% S_t = A_t*Sigma_{t-1}*A_t + B_t *y_{t-1}*y_{t-1}'*B_t + C_t*C_t^{\top}
% Sigma_t = S_t

T= numStates;
paramSize=size(params);
d2=d*(d+1)/2;
numParams = d2+2*d;

if paramSize(1)==T %use t-indexed params
    pLength= prod(paramSize(2:end));
    assert(pLength==numParams+(3*options.isDynamic)+(options.dataNu>0),...
        'incorrect number of parameters given')
elseif paramSize(1)==1 % static -> expand
    params= repmat(params,[T 1]);    
end

% defaults
if ~isfield(options,'dataNu')
    options.dataNu = 0; %data is Multi-Normal given Sigma
end
if nargin<5
    startSigma=eye(d);
    startData = mvnrnd(zeros(1,d),startSigma,1);
end

% generate according to model
SigmaT = repmat(startSigma,[1 1 T]);
simData = repmat(startData,[T 1]);
%retain initial Sigma0 and X0
for t=2:T     
    if options.isDynamic % generate dynamic parameters 
        prevHypers = params(t,numParams+1:numParams+3);        
        p_alpha=repmat(prevHypers(:,3),[1 d]); %for A
        p_beta=repmat(prevHypers(:,2),[1 d]); %for B
        p_gamma=repmat(prevHypers(:,1),[1 d2]); %for C
        prevStd = [p_gamma p_beta p_alpha];
        
        params(t,1:numParams) = randn(size(params(t-1,1:numParams))).*prevStd ...
            + params(t-1,1:numParams);
    end
    
    curSigma = bmdcSigma(simData(t-1,:),params(t,:),SigmaT(:,:,t-1),options);
    SigmaT(:,:,t)=curSigma;
    
    if options.dataNu % multi-variate student-t
        curChol=chol(curSigma);
        simData(t,:) = (mvtrnd(eye(d),options.dataNu,1)*curChol/ ...
            (options.dataNu/(options.dataNu-2)));
    else
        simData(t,:) = mvnrnd(zeros(1,d),curSigma,1);
    end
end
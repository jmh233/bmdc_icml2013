function [newLatents qLatents newParams qParams]= ...
    bmdcParamProposal(data,prevLatents,prevParams,pfOptions,varargin)
%% model:
% Y_t ~ N(0,Sigma_t); or % Y_t ~ Student(nu,Sigma_t); 
% S_t = f(theta,Sigma_{t-1},y_{t-1}), where f(.) is BMDC setup
% 1. Sigma_t = S_t
% 2. Sigma_t ~ Wish(nu,S_t/nu)

%% arguments:
%   data - to allow hypers to depend on data. in practice not used but needed for optimal importance density
%   prevLatents - to allow params to depend on hidden layer. in practice not used
%   prevParams - previous params and hyperparams
%   pfOptions:
%     .numSamples
%     .modelOptions:
%       .dataNu
%       .isDynamic - params are time-varying

N= pfOptions.numParticles; %alias
[T d]=size(data);
d2=d*(d+1)/2;

isFull=0;
if isfield(pfOptions.modelOptions,'isFull') && pfOptions.modelOptions.isFull
    isFull=1;
    numEvoParams = d2+2*d^2; % numEvo: C triangular, A, B full
else
    numEvoParams = d2+2*d; %numEvo: C triangular, A, B diagonal
end
% params is [C B A, jitterParams, dataNu]
numParams = numEvoParams + 3*pfOptions.modelOptions.isDynamic + (pfOptions.modelOptions.dataNu>0);

isPrior=0;
if isempty(prevLatents) || isempty(prevParams)
    isPrior=1;
end

% default parameters for bmdc with appropriate sizes
if isempty(prevParams) %prior for bmdc evolution parameters
    if ~isFull
        B= ones(1,d)*.1; %prior mean
        A= ones(1,d)*.99; %prior mean
        C= vecTriu(chol(eye(d)-diag(B.^2)-diag(A.^2)))';
    else
        B= eye(d)*.1; %prior mean
        A= eye(d)*.99; %prior mean
        C= vecTriu(chol(eye(d)-B^2-A^2))';
        B = B(:)'; A=A(:)'; %flatten
    end

    if pfOptions.modelOptions.isDynamic
        taus = pfOptions.modelOptions.tau;
    else
        taus = zeros(1,0);
    end
    if pfOptions.modelOptions.dataNu
        dataNus = pfOptions.modelOptions.dataNu;
    else
        dataNus = zeros(1,0);
    end
    
    prevParams = repmat([C B A taus dataNus],N,1);    

else % sizing for initial params
    if isPrior %start with initial theta_1
        if pfOptions.modelOptions.isDynamic %propose newParams around prevParams
            prevParams = repmat(prevParams(1,:),[N 1]); %resize \theta_1
        else %static bmdc (Bayesian BEKK)
            prevParams = repmat(prevParams,[N 1]);
        end
    end
end

% gaussian exploration around last point for parameters 
if isfield(pfOptions,'delta')
    delta=pfOptions.delta;
else
    delta=.95;
end
a=(3*delta-1)/(2*delta);
h = sqrt(1-a^2);

% treat tau, dataNu appropriately
prevHypers=prevParams(:,numEvoParams+1:end); %on natural scale
if pfOptions.modelOptions.dataNu % dataNU >2 shifted for student-t
    prevHypers(:,end)=prevHypers(:,end)-2;
end

% params prior lognormal distributed
if isPrior
    hyperStd=ones(size(prevHypers))*1;
    if pfOptions.modelOptions.dataNu
        hyperStd(:,end)=1.5; % might want exponential instead of logNormal for df
    end
else
    prevStd = std(log(prevHypers));
    hyperStd=repmat(prevStd,[N 1])*h; %need prevHypers on log scale
end
newHypers=randn(N,size(prevHypers,2)).*hyperStd+log(prevHypers);
qHypers = sum(normpdfln(newHypers,prevHypers,hyperStd),2);

newHypers = exp(newHypers); %exponentiate to natural scale
if pfOptions.modelOptions.dataNu % dataNU >2 shifted
    newHypers(:,end)=newHypers(:,end)+2;
end

% proposal for evolution parameters A, B, C given hypers
if pfOptions.modelOptions.isDynamic
    if isFull
        alpha=repmat(newHypers(:,3),[1 d^2]); %for A
        beta=repmat(newHypers(:,2),[1 d^2]); %for B
    else
        alpha=repmat(newHypers(:,3),[1 d]); %for A
        beta=repmat(newHypers(:,2),[1 d]); %for B
    end
    gamma=repmat(newHypers(:,1),[1 d2]); %for C
    jitters = [gamma beta alpha];
    prevStd=jitters;
elseif isPrior % widish gaussian prior if isPrior
    prevStd=ones(size(prevParams(:,1:numEvoParams)))*.3;
else % otherwise gaussian shrinkage on Bayesian BEKK version of BMDC
    prevStd=repmat(std(prevParams(:,1:numEvoParams)), [N 1])*h;
end
newParams = randn(N,numEvoParams).*prevStd+prevParams(:,1:numEvoParams);
qParams = sum(normpdfln(newParams,prevParams(:,1:numEvoParams),prevStd),2);

% generate latents sigma layer
if isPrior || isempty(prevLatents) 
    empSig = cov(data); %seed with empirical covariance
    initSig = empSig;
    prevLatents = zeros(d,d,N); 
    [initSigChol,isPosFlag] = chol(initSig/d);
    for n=1:N
        [a,blah] = wishrnd(initSig,d,initSigChol);
        prevLatents(:,:,n) = a;
    end
    prevData = data(1,:);
else
    prevLatents = reshape(prevLatents,[N d d]);
    if d==1
        prevLatents=shiftdim(prevLatents,-2);
    else
        prevLatents=shiftdim(prevLatents,1);
    end
    prevData = data(end-1,:);
end

% construct newLatents from prevLatents, data, newParams
newLatents = bmdcSigma(prevData,newParams,prevLatents,pfOptions.modelOptions);
qLatents = zeros(N,1); %deterministic

% package evolution params and hypers for output
newParams=[newParams, newHypers];
qParams = qParams+qHypers;
newLatents = shiftdim(newLatents,2); %conform with N*latentSize 